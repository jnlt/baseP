package com.basep.framework.shiro;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.filter.authc.UserFilter;
import org.apache.shiro.web.util.WebUtils;

import com.basep.framework.config.StaticConstants;

/**
 * user filter 主要为了控制不同类型用户的跳转,废弃 chenz 2014年12月5日14:45:30
 * @author Chenz
 * @date 2014-11-6 16:32:59
 *
 */
public class SystemUserFilter extends UserFilter {
	//未登录重定向到登陆页
	protected void redirectToLogin(ServletRequest req, ServletResponse resp) throws IOException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;
		String loginUrl;
		//后台地址跳转到后台登录地址，前台需要登录的跳转到shiro配置的登录地址
		if (request.getRequestURI().startsWith(request.getContextPath() + StaticConstants.ADMIN_PREFIX)) {
			loginUrl = StaticConstants.ADMIN_LOGIN_URL;
		} else if (request.getRequestURI().startsWith(request.getContextPath() + StaticConstants.CONTRACTOR_PREFIX)) {
			loginUrl = StaticConstants.CONTRACTOR_LOGIN_URL;
		} else {
			loginUrl = getLoginUrl();
		}
		WebUtils.issueRedirect(request, response, loginUrl);
	}

}
