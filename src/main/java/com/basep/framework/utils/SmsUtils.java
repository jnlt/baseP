package com.basep.framework.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**   
 * @Description: TODO 发送短信的Utils包
 * @author tanliansheng
 * @date 2014年11月19日 上午9:34:27 
 * @version V1.0
 * 	接口传过来的代码:
		200	发送短信成功
		512	服务器拒绝访问，或者拒绝操作
		513	求Appkey不存在或被禁用。
		514	权限不足
		515	服务器内部错误 
		517	缺少必要的请求参数
		518	请求中用户的手机号格式不正确（包括手机的区号）
		519	请求发送验证码次数超出限制
		520	无效验证码。
		526	余额不足
		
		服务器状态监测接口
		请求地址: https://120.132.154.117:8443/check/status
 * 
 * 
*/
public class SmsUtils {
	// 发送短信的接口
	private static final String sendSmsUrl= "https://api.sms.mob.com/sms/sendmsg";
	// 验证短信的接口
	private static final String checkCodeUrl = "https://api.sms.mob.com/sms/checkcode";
	// 应用的appkey 暂且使用的是安卓版的.
	private static final String content = "appkey=#&zone=86&phone=";
	
	private static final String charset = "UTF-8";
	
	public static void main(String[] args) throws Exception {
		
		
//		System.out.println("发送验证码给手机--->返回状态是200表示成功-->"+sendSms("#"));//{"status":200}
		
		
//		System.out.println("手机接收的验证码用接口验证一下,状态是200表示成功---->"+checkSms("#","8201"));
		
		
	}
	
	/**
	 * @Description: TODO 往手机号里发送验证码
	 * @param phone 手机号
	 * @return// {"status":200}
	 * update: 截取字符串 只返回三位的数字
	 */
	public static String sendSms(String phone){
		try {
			byte[] b = post(sendSmsUrl,content+phone, charset);
			return new String(b).substring(10, 13);
		} catch (Exception e) {
			e.printStackTrace();
			return "999";
		}
	}
	
	/**
	 * @Description: TODO
	 * @param phone 手机号
	 * @param code 用户填写的验证码
	 * @return// {"status":200}
	 * update: 截取字符串 只返回三位的数字
	 */
	public static String checkSms(String phone,String code){
		try {
			byte[] b = post(checkCodeUrl,content+phone+"&code="+code, charset);
			return new String(b).substring(10, 13);
		} catch (Exception e) {
			e.printStackTrace();
			return "999";
		}
	}
	
	
	private static class TrustAnyTrustManager implements X509TrustManager {
		public void checkClientTrusted(X509Certificate[] chain, String authType)
				throws CertificateException {
		}

		public void checkServerTrusted(X509Certificate[] chain, String authType)
				throws CertificateException {
		}

		public X509Certificate[] getAcceptedIssuers() {
			return new X509Certificate[] {};
		}
	}

	private static class TrustAnyHostnameVerifier implements HostnameVerifier {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

	/**
	 * post方式请求服务器(https协议)
	 * 
	 * @param url
	 *            请求地址
	 * @param content
	 *            参数
	 * @param charset
	 *            编码
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 * @throws IOException
	 * @throws NoSuchProviderException
	 */
	public static byte[] post(String url, String content, String charset)
			throws NoSuchAlgorithmException, KeyManagementException,
			IOException, NoSuchProviderException {
		try {
			TrustManager[] tm = { new TrustAnyTrustManager() };
			// SSLContext sc = SSLContext.getInstance("SSL");
			SSLContext sc = SSLContext.getInstance("SSL", "SunJSSE");
			sc.init(null, tm, new java.security.SecureRandom());
			URL console = new URL(url);

			HttpsURLConnection conn = (HttpsURLConnection) console
					.openConnection();
			conn.setSSLSocketFactory(sc.getSocketFactory());
			conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setRequestMethod("POST");
			conn.connect();
			DataOutputStream out = new DataOutputStream(conn.getOutputStream());
			out.write(content.getBytes(charset));
			// 刷新、关闭
			out.flush();
			out.close();
			InputStream is = conn.getInputStream();
			if (is != null) {
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = is.read(buffer)) != -1) {
					outStream.write(buffer, 0, len);
				}
				is.close();
				return outStream.toByteArray();
			}
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}
